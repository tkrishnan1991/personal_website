---
title: RESUME
description: Resume Section
---

Feel free to check out my 3 resumes here:

- <a href="https://tara-website.s3-us-west-2.amazonaws.com/TaraKrishnanResume.pdf">General Resume</a>
- <a href="https://tara-website.s3-us-west-2.amazonaws.com/TaraKrishnanResume+PM.pdf"</a>Product Manager Resume</a>
- <a href="https://tara-website.s3-us-west-2.amazonaws.com/TaraKrishnanResumePMM.pdf"</a>Product Marketing Resume</a>

