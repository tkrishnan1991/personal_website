---
title: Home
description: Welcome to this sample project
images: ["/decipher_videos/decipher.png"]
---

Hey,

I'm Tara Krishnan, a product enthusiast driven by process automation and turning customer problems into actionable solutions via product messaging and product positioning. 

