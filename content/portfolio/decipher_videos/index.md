---
title: Decipher Videos with Bots
description: Transcribe MP4s with Amazon Transcribe 
date: "2018"
jobDate: June 2018 - May 2019
work: [Jama Software]
techs: [Google Cloud Computing (GCP), Amazon Web Services (AWS) Lambda and S3]
designs: [iMovie]
thumbnail: decipher_videos/decipher.png
projectUrl: https://rebrand.ly/deciphervideoswbots
testimonial:
  name: David Shanley
  role: VP Product Marketing @ Jama Software
  image: decipher_videos/dave.jpeg
  text: Tara brings a hard-to-find mix of market awareness, technical knowledge, and a desire to learn along with a can-do attitude and perseverance.
---

Companies which host videos online come to a crossroads when deciding what content must stay and what can go. To approach this problem I created a function that would transcribe videos into text. It can benefit those needing to quickly transcribe videos and those needing to filter video content on keywords.

