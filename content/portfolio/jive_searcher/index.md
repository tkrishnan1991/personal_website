---
title: Scrape Through Jive
description: Quickly filter Jive content using Jive's API
date: "2019"
jobDate: June 2018 - May 2019
work: [Jama Software]
techs: [Jive API, React, Netlify]
designs: 
thumbnail: jive_searcher/jive_searcher.png
projectUrl: 
testimonial:
  name: David Shanley
  role: VP Product Marketing @ Jama Software
  image: decipher_videos/dave.jpeg
  text:  Her [Tara] strong sense of ownership, high level of organization, and desire to continually learn meant that she meshed seamlessly with our deeply experienced Product Marketing team.
---

Companies host a lot of content on Jive, but Jive's search function is not very user friendly. To approach this problem I used React to create a user friendly "keyword search" filter function. 

