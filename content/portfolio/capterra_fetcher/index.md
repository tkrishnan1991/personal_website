---
title: Slack a Capterra Review
description: Slack Notification for Capterra Review
date: "2018"
jobDate: June 2018 - May 2019
work: [Jama Software]
techs: [Amazon Web Services (AWS) Lambda and S3, Slack API]
designs:
thumbnail: capterra_fetcher/capterra_fetcher.png
projectUrl: 
testimonial:
  name: David Shanley
  role: VP Product Marketing @ Jama Software
  image: capterra_fetcher/dave.jpeg
  text: Tara owned messaging on the Customer Review sites (like Capterra, G2 Crowd) and developing an incentive program to yield more reviews in a scrappy, low-cost way.
---

After a customer writes a customer review on Capterra, Slack sends an instant notification showcasing the reviewer name, NPS score and total number of reviews to date allowing companies to stay up-to date on sales leads and approach critical feedback immediately. 
