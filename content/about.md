---
title: ABOUT
description: Hey, I'm Tara Krishnan
images: []

---

Hi, I'm Tara! I'm a product enthusiast driven by process automation and turning customer problems into actionable solutions via product messaging and product positioning. 

I have a mix of marketing, sales and technical skills. I have certifications in Pragmatic Marketing (Level III) and Scrum Ownership. Feel free to check out my resume <a href="https://tara-website.s3-us-west-2.amazonaws.com/TaraKrishnanResume.pdf">here</a>.

I volunteer with STEM Like a Girl, a non-profit dedicated to inspiring young girls to develop interest and confidence in STEM through hands on activities and parent engagement, as well as ChickTech, a non-profit whose mission is to engage women of all ages in the tech industry.

In addition, I enjoy barre, running, and cycling. My husband and I just bought a townhouse which we are currently in the process of remodeling too! 



